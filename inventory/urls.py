from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('inventorys/', views.InventoryListView.as_view(), name='inventorys'),
    path('inventory/<int:pk>', views.InventoryDetailView.as_view(), name='inventory-detail')
]
