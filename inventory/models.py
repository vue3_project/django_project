from django.db import models
from django.urls import reverse  # To generate URLS by reversing URL patterns

# Create your models here.

class InventoryGenre(models.Model):
    """Model representing a inventory genre (e.g. Drinks, fruits)."""
    name = models.CharField(
        max_length=200,
        help_text="Enter a inventory genre (e.g. Drinks, fruits etc.)"
        )

    def __str__(self):
        """String for representing the Model object (in Admin site etc.)"""
        return self.name


class Inventory(models.Model):
    """Model representing a Inventory (but not a specific copy of a Inventory)."""

    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    note = models.TextField(max_length=1000, help_text="Enter a brief description of the inventory")
   
    genre = models.ManyToManyField(InventoryGenre, help_text="Select a inventory genre for this inventory")
    # ManyToManyField used because a genre can contain many inventorys and a Inventory can cover many genres.
    # InventoryGenre class has already been defined so we can specify the object above.
    
    
    class Meta:
        ordering = ['name', 'description']
        #ordering = ['name']

    def display_genre(self):
        """Creates a string for the Inventory Genre. This is required to display inventory genre in Admin."""
        return ', '.join([genre.name for genre in self.genre.all()[:3]])

    display_genre.short_description = 'InventoryGenre'

    def get_absolute_url(self):
        """Returns the url to access a particular inventory instance."""
        return reverse('inventory-detail', args=[str(self.id)])

    def __str__(self):
        """String for representing the Model object."""
        return self.name

import uuid  # Required for unique inventory instances

class InventoryInstance(models.Model):
    """Model representing a specific copy of a inventory (i.e. that can be sell from the stock)."""
    id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                          help_text="Unique ID for this particular inventory across whole stock")
    inventory = models.ForeignKey('Inventory', on_delete=models.RESTRICT, null=True)
    


    STOCK_STATUS = (        
        ('a', 'Available'),
        ('u', 'Unavailable'),
    )

    status = models.CharField(
        max_length=1,
        choices=STOCK_STATUS,
        blank=True,
        default='a',
        help_text='Inventory availability')

    class Meta:
        ordering = ['inventory']
        

    def __str__(self):
        """String for representing the Model object."""
        return '{0} ({1})'.format(self.id, self.inventory.name)


class Supplier(models.Model):
    """Model representing an supplier."""
    supplier_name = models.CharField(max_length=100)
   

    class Meta:
        ordering = ['supplier_name']

    def get_absolute_url(self):
        """Returns the url to access a particular supplier instance."""
        return reverse('supplier-detail', args=[str(self.id)])

    def __str__(self):
        """String for representing the Model object."""
        return self.supplier_name