from django.shortcuts import render

# Create your views here.

from .models import Inventory, Supplier, InventoryInstance, InventoryGenre

def index(request):
    """View function for home page of site."""

    # Generate counts of some of the main objects
    num_inventorys = Inventory.objects.all().count()
    num_instances = InventoryInstance.objects.all().count()

    # Available inventorys (status = 'a')
    num_instances_available = InventoryInstance.objects.filter(status__exact='a').count()

    # The 'all()' is implied by default.
    num_suppliers = Supplier.objects.count()

    context = {
        'num_inventorys': num_inventorys,
        'num_instances': num_instances,
        'num_instances_available': num_instances_available,
        'num_suppliers': num_suppliers,
    }

    # Render the HTML template index.html with the data in the context variable
    return render(request, 'index.html', context=context)

from django.views import generic

class InventoryListView(generic.ListView):
    model = Inventory


class InventoryDetailView(generic.DetailView):
    model = Inventory

