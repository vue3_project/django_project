from django.contrib import admin

# Register your models here.

from .models import Supplier, InventoryGenre, Inventory, InventoryInstance

admin.site.register(Supplier)
admin.site.register(InventoryGenre)
admin.site.register(Inventory)
admin.site.register(InventoryInstance)